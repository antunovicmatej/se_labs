# Najbolji Git tutorijal


test

## git status
prikazuje status svih datoteka

## git add
dodavanje novih datoteka u sustav za praćenje

## git log
prikazuje sve izmjene u repozitoriju

## git commit
spremanje promjena nad datotekama

## git diff
ispisuje sve promjene napravljene od posljednjeg commita

## git push
uploadanje lokalnog repozitorija na gitlab

## git config
postavljanje korisničkih podataka za repozitorij

## git pull
povlači promjene sa udaljenog repozitorija na naš lokalni repozitorij
koristi se na način:

```
git pull origin master
```

gdje `origin` predstavlja naziv udaljenog repozitorija, a `master` predstavlja naziv brancha
