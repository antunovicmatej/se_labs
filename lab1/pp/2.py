# Ask the user for a number. 
# Depending on whether the number is even or odd, 
# print out an appropriate message to the user.

num = int(input("Enter first num: "))
check = int(input("Enter second num: "))

if num % check == 0:
    print(str(num) + " is divisible by " + str(check))
else:
    print(str(num) + " is not divisible by " + str(check))


'''
if num % 4 == 0:
    print("Broj je visekratnik broja 4")
elif num % 2 == 0:
    print("Broj je paran")
else:
    print("Broj je neparan")
'''