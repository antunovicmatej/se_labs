import random

random_num = random.randint(1, 9)
guess = int(input("Guess the number "))
print(guess)

while(guess != random_num):
    if(guess > random_num):
        print("Too high, you missed by " + str(int(guess) - int(random_num)))
    else:
        print("Too low, you missed by " + str(int(random_num) - int(guess)))
    guess = int(input("Guess the number "))
print("Game finished.")
