401  cd lab5
open folder you made

403  python -m venv django
make virtual environment

405  source django/Scripts/activate
activate virtual environment

406  pip install django
install django in previously made folder

407  django-admin --version
check if django's version is alright

408  django-admin startproject myimgur
make a new django project named myimgur

410  cd myimgur/
open previously made project folder

412  winpty python manage.py runserver
start server so you can see if you made it work right

413  cd ..
414  cd ..
go to the se_labs folder so you can see the status of your project

415  git status
419  git add lab5
420  git status
421  git commit -m "Add myimgur initial project to lab5"
commit the new folder you made

422  git add .gitignore
add gitignore

423  git status
424  git commit --amend
commit gitignore but without name

425  history

429  cd lab5
go in lab5 folder so you can see the versions

430  ls
431  pip freeze
it writes versions of django and the rest in the console

432  pip freeze > requirements.txt
it writes versions of django and the rest in the requirements.txt file in lab5 folder

433  ls
434  pip install -r requirements.txt
install every version thats in requirements.txt

